<?php

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user           = new User();
        $user->email    = 'admin@twojafirma.pl';
        $user->password = Hash::make('admin123');
        $user->save();
    }
}