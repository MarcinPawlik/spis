<?php

class WorkerTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user           = new Worker();
        $user->name     = 'Jan';
        $user->surename = 'Kowalski';
        $user->email    = 'jan.kowalski@twojafirma.pl';
        $user->save();

        $user           = new Worker();
        $user->name     = 'Przemysław';
        $user->surename = 'Janowski';
        $user->email    = 'przemyslaw.janowski@twojafirma.pl';
        $user->save();

        $user           = new Worker();
        $user->name     = 'Andrzej';
        $user->surename = 'Nowak';
        $user->email    = 'andrzej.nowak@twojafirma.pl';
        $user->save();
    }
}