<?php

Route::model('item', 'Item');
Route::model('worker', 'Worker');

Route::bind('itemWithTrashed', function($value)
{
    return Item::withTrashed()->where('id', $value)->firstOrFail();
});
Route::bind('workerWithTrashed', function($value)
{
    return Worker::withTrashed()->where('id', $value)->firstOrFail();
});


Route::get('/auth', 'AuthController@index')->before('guest');
Route::post('/auth', 'AuthController@login')->before('guest');
Route::get('/auth/logout', 'AuthController@logout')->before('auth');


Route::group(array('before' => 'auth'), function(){

    Route::get('/', 'WorkerController@index');
    Route::post('/add', 'WorkerController@addItem');
    Route::get('/remove/{item}', 'WorkerController@deleteItem');
    Route::get('/delete/{worker}', 'WorkerController@delete');
    Route::get('/restore/{workerWithTrashed}', 'WorkerController@restore');
    Route::get('worker/create', 'WorkerController@create');
    Route::post('worker/create', 'WorkerController@store');
    Route::get('worker/edit/{workerWithTrashed}', 'WorkerController@edit');
    Route::post('worker/edit/{workerWithTrashed}', 'WorkerController@update');

    Route::get('/items', 'ItemController@index');
    Route::get('/items/show/{item}', 'ItemController@show');
    Route::get('/items/create', 'ItemController@create');
    Route::post('/items/create', 'ItemController@store');
    Route::get('/items/edit/{item}', 'ItemController@edit');
    Route::post('/items/edit/{item}', 'ItemController@update');
    Route::get('/items/delete/{item}', 'ItemController@delete');
    Route::get('/items/restore/{itemWithTrashed}', 'ItemController@restore');
    Route::get('/items/worker/delete/{worker}', 'ItemController@deleteWorker');

});


