<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Logowanie</title>
    <link href="{{ URL::to('css/bootstrap.min.css') }}" rel="stylesheet">

  </head>

  <body>

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-wrap">
                    <h1>Logowanie</h1>
                        <form role="form" method="post" id="login-form" autocomplete="off">
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input name="email" id="email" class="form-control" placeholder="email@example.com" type="email">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Hasło</label>
                                <input name="password" id="password" class="form-control" placeholder="Hasło" type="password">
                            </div>
                            <input id="btn-login" class="btn btn-custom btn-lg btn-block btn-success" value="Logowanie" type="submit">
                        </form>
                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
  </body>
</html>
