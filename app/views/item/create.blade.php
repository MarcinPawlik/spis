@extends('layout/master')

@section('content')
<h3>Nowy przedmiot</h3>


<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    @foreach($data as $key => $value)
      @if(reset($data) == $value)
        <li role="presentation" class="active"><a href="#{{ $key }}" aria-controls="{{ $key }}" role="tab" data-toggle="tab">{{ $value['name'] }}</a></li>
      @else
        <li role="presentation"><a href="#{{ $key }}" aria-controls="{{ $key }}" role="tab" data-toggle="tab">{{ $value['name'] }}</a></li>
      @endif
    @endforeach
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    @foreach($data as $key => $value)
        <div role="tabpanel" class="tab-pane {{ (reset($data) == $value)?'active':'' }}" id="{{ $key }}">
        <br/>
        {{ Form::open(['action' => 'ItemController@store', 'class' => 'form-horizontal']) }}

          @foreach($value['values'] as $k => $v)

            <div class="form-group">
              <label for="input_{{ $k }}" class="col-sm-3 control-label">{{ $v['lbl'] }}</label>
              <div class="col-sm-9">
                @if($v['type'] == 'textarea')
                  <textarea class="form-control" id="input_{{ $k }}" name="{{ $k }}">{{ Input::has($k) ? Input::get($k) : $v['value'] }}</textarea>
                @else
                 <input type="text" class="form-control" id="input_{{ $k }}" name="{{ $k }}" value="{{ Input::has($k) ? Input::get($k) : $v['value'] }}" />
                @endif
              </div>
            </div>

          @endforeach

          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <input type="hidden" name="type" value="{{ $key }}" />
              <button type="submit" class="btn btn-success">Dodaj</button>
            </div>
          </div>

        {{ Form::close() }}


        </div>
    @endforeach
  </div>

</div>


@stop
