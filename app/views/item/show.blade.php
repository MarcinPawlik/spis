@extends('layout/master')

@section('content')
<h3>Szczegóły przedmiotu</h3>


<style>
  .input{
    padding: 6px 12px;
  }
</style>



        <br/>
        <div class="form-horizontal">

          @foreach($data['values'] as $k => $v)

            <div class="form-group">
              <label for="input_{{ $k }}" class="col-sm-3 control-label">{{ $v['lbl'] }}</label>
              <div class="col-sm-9">
                <p class="input">
                  {{ nl2br(htmlspecialchars($v['value'])) }}
                </p>
              </div>
            </div>

          @endforeach


        {{ Form::close() }}


        </div>




@stop
