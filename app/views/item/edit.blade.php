@extends('layout/master')

@section('content')
<h3>Edytuj przedmiot</h3>






        <br/>
        {{ Form::open(['action' => ['ItemController@update', $item->id], 'class' => 'form-horizontal']) }}

          @foreach($data['values'] as $k => $v)

            <div class="form-group">
              <label for="input_{{ $k }}" class="col-sm-3 control-label">{{ $v['lbl'] }}</label>
              <div class="col-sm-9">
                @if($v['type'] == 'textarea')
                  <textarea class="form-control" id="input_{{ $k }}" name="{{ $k }}">{{ htmlspecialchars(Input::has($k) ? Input::get($k) : $v['value']) }}</textarea>
                @else
                 <input type="text" class="form-control" id="input_{{ $k }}" name="{{ $k }}" value="{{ htmlspecialchars(Input::has($k) ? Input::get($k) : $v['value']) }}" />
                @endif
              </div>
            </div>

          @endforeach

          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <input type="hidden" name="type" value="{{ $key }}" />
              <button type="submit" class="btn btn-success">Edytuj</button>
            </div>
          </div>

        {{ Form::close() }}






@stop
