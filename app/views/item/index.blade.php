@extends('layout/master')

@section('content')
<h3>Lista przedmiotów</h3>

<div class="text-right">
  <a class="btn btn-success" href="{{ action('ItemController@create') }}">Nowy przedmiot</a>
</div>

<br>

@if(Session::has('success'))
  <div class="alert alert-success" role="alert">
    {{ Session::get('success') }}
  </div>
@endif

@if(Session::has('error'))
  <div class="alert alert-danger" role="alert">
    <span class="sr-only">Error:</span>
    {{ Session::get('error') }}
  </div>
@endif

<table class="table table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>Typ</th>
          <th>Przedmiot</th>
          <th>Pracownik</th>
          <th>Opcje</th>

        </tr>
      </thead>
      <tbody>
        @foreach($items as $item)

            @if($item->deleted_at == NULL)
              <tr>
            @else
              <tr class="danger">
            @endif
              <th scope="row">{{  $item->id }}</th>
              <td>{{ $item_array[$item->value]['name'] }}</td>
              <td><a href="{{ action('ItemController@show', $item->id) }}">{{ htmlspecialchars($item->name) }}</a></td>
              <td>
                @if($item->worker()->exists())
                {{ $item->worker()->first()->name }} {{ $item->worker()->first()->surename }}
                <a href="{{ action('ItemController@deleteWorker', $item->worker()->first()->id) }}"><i class=" glyphicon glyphicon-remove"></i></a>
                @else
                ---
                @endif

              </td>
              <td>
                <a class="btn btn-info btn-sm" href="{{ action('ItemController@edit', $item->id) }}">Edytuj</a>
                @if($item->deleted_at == NULL)
                  <a class="btn btn-danger btn-sm" href="{{ action('ItemController@delete', $item->id) }}">Usuń</a>
                @else
                  <a class="btn btn-success btn-sm" href="{{ action('ItemController@restore', $item->id) }}">Przywróć</a>
                @endif

              </td>

            </tr>
        @endforeach
      </tbody>
    </table>


@stop
