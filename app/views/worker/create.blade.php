@extends('layout/master')

@section('content')
<h3>Nowy pracownik</h3>



@if(Session::has('message'))

  <div class="alert alert-danger" role="alert">
    <ul>
    @foreach(Session::get('message')->all() as $msg)
        <li>{{ $msg }}</li>
    @endforeach
    </ul>
  </div>

@endif

{{ Form::open(['action' => 'WorkerController@store', 'class' => 'form-horizontal']) }}

<div class="form-group">
  <label for="input_name" class="col-sm-3 control-label">Imie</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="input_name" name="name" value="{{ Session::has('name') ? Session::get('name') : '' }}" />
  </div>
</div>

<div class="form-group">
  <label for="input_surename" class="col-sm-3 control-label">Nazwisko</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="input_surename" name="surename" value="{{ Session::has('surename') ? Session::get('surename') : '' }}" />
  </div>
</div>

<div class="form-group">
  <label for="input_email" class="col-sm-3 control-label">Adres email</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="input_email" name="email" value="{{ Session::has('email') ? Session::get('email') : '' }}" />
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-9">
    <button type="submit" class="btn btn-success">Dodaj</button>
  </div>
</div>

{{ Form::close() }}


@stop
