@extends('layout/master')

@section('content')
<h3>Lista pracowników</h3>

<div class="text-right">
  <a class="btn btn-success" href="{{ action('WorkerController@create') }}">Nowy pracownik</a>
</div>

<table class="table table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>Imie</th>
          <th>Nazwisko</th>
          <th>Email</th>
          <th>Posiadane przedmioty</th>
          <th>Opcje</th>
        </tr>
      </thead>
      <tbody>
        @foreach($workers as $worker)

            @if($worker->deleted_at == NULL)
              <tr>
            @else
              <tr class="danger">
            @endif
              <th scope="row">{{  $worker->id }}</th>
              <td>{{  $worker->name }}</td>
              <td>{{  $worker->surename }}</td>
              <td>{{  $worker->email }}</td>
              <td>
                <ul class="list-unstyled">
                  @if($worker->deleted_at == NULL)
                    @foreach($worker->items as $i)
                        <li>
                          <a href="{{ action('ItemController@show', $i->id) }}">{{ htmlspecialchars($i->name) }}</a>
                          <a href="{{ action('WorkerController@deleteItem', $i->id) }}"><i class=" glyphicon glyphicon-remove"></i></a>

                        </li>
                    @endforeach

                  </ul>
                  <a href="#" class="openModal" data-worker="{{ $worker->id }}" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i></a>
                  @endif
              </td>
              <td>
                <a href="{{ action('WorkerController@edit', $worker->id) }}" class="btn btn-sm btn-info">Edytuj</a>
                @if($worker->deleted_at == NULL)
                  <a href="{{ action('WorkerController@delete', $worker->id) }}" class="btn btn-sm btn-danger">Usuń</a>
                @else
                  <a href="{{ action('WorkerController@restore', $worker->id) }}" class="btn btn-sm btn-success">Przywróć</a>
                @endif
              </td>
            </tr>
        @endforeach
      </tbody>
    </table>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ Form::open(['action' => 'WorkerController@addItem', 'class' => 'form-horizontal']) }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Przydziel przedmiot</h4>
        </div>
        <div class="modal-body">

            <div class="form-group">
              <label for="input_select" class="col-sm-3 control-label">Wybierz</label>
              <div class="col-sm-9">
                <select id="input_select" class="form-control" name="item">
                  @foreach($items as $item)
                    @if(!$item->worker()->exists())
                     <option value="{{ $item->id }}">{{ htmlspecialchars($item->name) }}</option>
                     @endif
                  @endforeach
                </select>

              </div>
            </div>



          <input id="id_worker" name="worker" type="hidden" value="0" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          <button type="submit" class="btn btn-primary">Przydziel</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>

{{ HTML::script('js/jquery-2.1.3.min.js') }}

<script type="text/javascript">

$( document ).ready(function(){

  $('.openModal').click(function(){
    var id_worker = $(this).attr('data-worker');
    console.log(id_worker);
    $('#id_worker').val(id_worker);
  });

});

</script>

@stop
