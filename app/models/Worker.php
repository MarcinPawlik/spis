<?php

class Worker extends Eloquent {

    use SoftDeletingTrait;

	protected $table = 'workers';

    public function items()
    {
        return $this->belongsToMany('Item', 'worker_item');
    }

}
