<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Item extends Eloquent {

    use SoftDeletingTrait;

	protected $table = 'items';

    public function worker()
    {
        return $this->belongsToMany('Worker', 'worker_item');
    }

}
