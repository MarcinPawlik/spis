<?php

class AuthController extends BaseController {


	public function index()
	{
		return View::make('login');
	}

	public function login(){
		$email = Input::get('email');
		$password = Input::get('password');
		if (Auth::attempt(array('email' => $email, 'password' => $password)))
		{
		    return Redirect::to('/');
		}
		return Redirect::action('AuthController@index');
	}

	public function logout(){
		Auth::logout();
		return Redirect::to('/');
	}

}
