<?php

class ItemController extends BaseController {

    /*
    * Wyświetla liste przedmiotów
    */
	public function index(){
        $items = Item::withTrashed()->where('parent_id', 0)->orderBy('deleted_at')->get();
		return View::make('item/index')
                ->withItems($items)
                ->withItemArray($this->itemArray());
	}

    /*
    * Wyświetla przedmiot
    */
    public function show($item){
        $itemArray = $this->itemArray();
        $dataArray = $itemArray[$item->value];

        $items = Item::where('parent_id', $item->id)->get();
        foreach ($items as $value) {
            $dataArray['values'][$value->name]['value'] = $value->value;
        }

        return View::make('item/show')
                ->withData($dataArray)
                ->withKey($item->value)
                ->withItem($item);
    }

    /*
    * Wyświetla formularz dodawania przedmiotów
    */
    public function create(){
        return View::make('item/create')
                ->withData($this->itemArray());
    }

    /*
    * Dodawanie przedmiotu
    */
    public function store(){
        $data = Input::except(['_token', 'type']);

        /* def. nazwy dla typu */
        $type = Input::get('type');

        if(!array_key_exists( $type , $this->itemArray())){
            return Redirect::action('ItemController@index')
                    ->withDanger('Niepoprawny typ.');
        }

        $name = $this->setName($type, $data);

        $firstItem = new Item();
        $firstItem->name = $name;
        $firstItem->value = $type;
        $firstItem->save();

        foreach($data as $key => $value){
            $item = new Item();
            $item->name = $key;
            $item->value = $value;
            $item->parent_id = $firstItem->id;
            $item->save();
        }


        return Redirect::action('ItemController@index')
                ->withSuccess('Dodano nowy przedmiot');
    }

    /*
    * Wyświetla formularz edytowania przedmiotów
    */
    public function edit($item){

        $itemArray = $this->itemArray();
        $dataArray = $itemArray[$item->value];

        $items = Item::where('parent_id', $item->id)->get();
        foreach ($items as $value) {
            $dataArray['values'][$value->name]['value'] = $value->value;
        }

        return View::make('item/edit')
                ->withData($dataArray)
                ->withKey($item->value)
                ->withItem($item);
    }

    /*
    * Edytuje przedmiot
    */
    public function update($item){
        $data = Input::except(['_token', 'type']);

        /* def. nazwy dla typu */
        $type = Input::get('type');

        if(!array_key_exists( $type , $this->itemArray())){
            return Redirect::action('ItemController@index')
                    ->withDanger('Niepoprawny typ.');
        }

        $name = $this->setName($type, $data);


        Item::where('parent_id', $item->id)->forceDelete(); //usuwa wpisy dla przedmiowu
        $item->name = $name;
        $item->save(); //aktualizacja nazwy z nowych danych

        /* wpisuje nowe dane na nowo */
        foreach($data as $key => $value){
            $newItem = new Item();
            $newItem->name = $key;
            $newItem->value = $value;
            $newItem->parent_id = $item->id;
            $newItem->save();
        }


        return Redirect::action('ItemController@index')
                ->withSuccess('Zmodyfikowano przedmiot');
    }


    /*
    * Usuwa przedmiot
    */
    public function delete($item){
        $item->delete();
        return Redirect::back();
    }

    /*
    * Przywraca przedmiot
    */
    public function restore($item){
        $item->restore();
        return Redirect::back();
    }

    /*
    * Zabiera przedmiot pracownikowi
    */
    public function deleteWorker($worker){
        WorkerItem::where('worker_id', $worker->id)->delete();
        return Redirect::back();
    }

    /* Tablica def. typy przedmiotów i ich pola */
    private function itemArray(){
        //pole type jest zarezerwowane !!!!!
        return [
        'car' => [
            'name' => 'Samochód',
            'values' => [
                'model' => ['lbl' => 'Model', 'type' => 'input', 'value' => ''],
                'car_number' => ['lbl' => 'Rejestracja', 'type' => 'input', 'value' => ''],
                'vin_number' => ['lbl' => 'VIN', 'type' => 'input', 'value' => ''],
                'car_engine' => ['lbl' => 'Silnik', 'type' => 'input', 'value' => ''],
                'buy_date' => ['lbl' => 'Data zakupu', 'type' => 'input', 'value' => ''],
                'review_date' => ['lbl' => 'Data przeglądu', 'type' => 'input', 'value' => ''],
                'insurance_date' => ['lbl' => 'Data ubezpieczenia', 'type' => 'input', 'value' => ''],
                'leasing_start_date' => ['lbl' => 'Data rozpoczęcia leasingu', 'type' => 'input', 'value' => ''],
                'leasing_end_date' => ['lbl' => 'Data zakończenia leasingu', 'type' => 'input', 'value' => ''],
                'month_cost' => ['lbl' => 'Miesięczny koszt', 'type' => 'input', 'value' => ''],
                'other' => ['lbl' => 'Inne', 'type' => 'textarea', 'value' => ''],
                ]

            ],
        'phone' => [
            'name' => 'Telefon',
            'values' => [
                'model' => ['lbl' => 'Model', 'type' => 'input', 'value' => ''],
                'serial_number' => ['lbl' => 'Numer Seryjny', 'type' => 'input', 'value' => ''],
                'phone_number' => ['lbl' => 'Numer Telefonu', 'type' => 'input', 'value' => ''],
                'subscription_start_date' => ['lbl' => 'Data rozpoczęcia abonamentu', 'type' => 'input', 'value' => ''],
                'subscription_end_date' => ['lbl' => 'Data zakończenia abonamentu', 'type' => 'input', 'value' => ''],
                'month_cost' => ['lbl' => 'Miesięczny koszt', 'type' => 'input', 'value' => ''],
                'other' => ['lbl' => 'Inne', 'type' => 'textarea', 'value' => ''],
                ]
            ],
        'laptop' => [
            'name' => 'Laptop',
            'values' => [
                'model' => ['lbl' => 'Nazwa', 'type' => 'input', 'value' => ''],
                'serial_number' => ['lbl' => 'Numer Seryjny', 'type' => 'input', 'value' => ''],
                'operation_system' => ['lbl' => 'System operacyjny', 'type' => 'input', 'value' => ''],
                'other' => ['lbl' => 'Inne', 'type' => 'textarea', 'value' => ''],
                ]
            ],
        'other' => [
            'name' => 'Inne',
            'values' => [
                'model' => ['lbl' => 'Nazwa', 'type' => 'input', 'value' => ''],
                'other' => ['lbl' => 'Inne', 'type' => 'textarea', 'value' => ''],
                ]
            ],
        ];
    }

    /* ustawia nazwe dla typu */
    public function setName($type, $data){
        if($type == 'car'){
            $name = $data['model'].' '.$data['car_engine'];
        }else if($type == 'phone'){
            $name = $data['model'].' '.$data['phone_number'];
        }else{
            $name = $data['model'];
        }
        return $name;
    }

}
