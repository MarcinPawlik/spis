<?php

class WorkerController extends BaseController {

    private $rules = [
            'name' => 'required|min:3|max:25|alpha',
            'surename' => 'required|min:3|max:25|alpha',
            'email' => 'required|email',
        ];

    //Wyświetla liste pracowników
	public function index(){
        $workers = Worker::withTrashed()->orderBy('deleted_at')->get();
        $items = Item::where('parent_id', 0)->get();
		return View::make('worker/index')
                ->withWorkers($workers)
                ->withItems($items);
	}

    //Wyświetla formularz dodawania pracownika
    public function create(){
        return View::make('worker/create');
    }

    //Dodaje pracownika
    public function store(){
        $data = Input::only('name', 'surename', 'email');

        $validator = Validator::make($data, $this->rules);
        if ($validator->fails()){
            return Redirect::back()
                    ->withMessage($validator->messages())
                    ->with($data);
        }

        $worker = new Worker();
        $worker->name = $data['name'];
        $worker->surename = $data['surename'];
        $worker->email = $data['email'];
        $worker->save();

        return Redirect::action('WorkerController@index');

    }

    //Wyświetla formularz edytowania pracownika
    public function edit($worker){
        return View::make('worker/edit')
                ->withWorker($worker);
    }

    //Edytuje pracownika
    public function update($worker){
        $data = Input::only('name', 'surename', 'email');

        $validator = Validator::make($data, $this->rules);
        if ($validator->fails()){
            return Redirect::back()
                    ->withMessage($validator->messages())
                    ->with($data);
        }


        $worker->name = $data['name'];
        $worker->surename = $data['surename'];
        $worker->email = $data['email'];
        $worker->save();

        return Redirect::action('WorkerController@index');

    }


    //Dodaje przedmiot do pracownika
    public function addItem(){
        $worker = Input::get('worker');
        $item = Input::get('item');


        $new = new WorkerItem();
        $new->item_id = $item;
        $new->worker_id = $worker;
        $new->save();

        return Redirect::back();
    }


    //Zabiera przedmiot pracownikowi
    public function deleteItem($item){
        WorkerItem::where('item_id', $item->id)->delete();

        return Redirect::back();
    }


    //Usuwa pracownika
    public function delete($worker){
        WorkerItem::where('worker_id', $worker->id)->delete();
        $worker->delete();
        return Redirect::back();
    }


    //Przywraca pracownika
    public function restore($worker){
        $worker->restore();
        return Redirect::back();
    }

}
